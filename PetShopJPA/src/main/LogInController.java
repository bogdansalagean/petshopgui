package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LogInController implements Initializable {

	@FXML private TextField username;
	@FXML private PasswordField password;
	@FXML private Button logIn;
	@FXML private Button exit;
	
	private Socket socket;
    private BufferedReader serverResponse;
    private PrintWriter loginDataProvider;
 
    private static final String loginAccepted = "Access granted";
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		/**
		 * making the connection to the server
		 */
		
		try {
            socket = new Socket("localhost", 5000);
            serverResponse = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            loginDataProvider =  new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Can't connect to server.");
        }
		
		/**
		 * checking if the data inserted into the cells matches the one in the server
		 */
		
			logIn.setOnMouseClicked(event -> {
		
				if (password.getText() != null && username.getText() != null) {
					
					String usernamename = username.getText();
					String pass = password.getText();
					
					loginDataProvider.println(usernamename);
					loginDataProvider.println(pass);
					/**
					 * if the data matches , swapping to the next scene
					 */
					try {
						if(serverResponse.readLine().equals(loginAccepted)) {
							Scene currentScene=logIn.getScene();
							currentScene.getWindow().hide();
							
							FXMLLoader fxmlLoader = new FXMLLoader();
							fxmlLoader.setLocation(getClass().getResource("DoctorInterface.fxml"));
							Scene scene = new Scene(fxmlLoader.load());
							Stage stage = new Stage();
							stage.setTitle("Pet Shop");
							stage.setScene(scene);
							stage.show();
						}
						/**
						 * if not we move to the error scene
						 */
						else {
							Scene currentScene=logIn.getScene();
							currentScene.getWindow().hide();
							
							FXMLLoader fxmlLoader = new FXMLLoader();
							fxmlLoader.setLocation(getClass().getResource("Error.fxml"));
							Scene scene = new Scene(fxmlLoader.load());
							Stage stage = new Stage();
							stage.setTitle("Acces denied!");
							stage.setScene(scene);
							stage.show();
						}
					    
					    } catch (IOException e) {
					    	System.out.println("Failed to create a new window.");
					    }
				}
			});
			
		exit.setOnAction(event -> Platform.exit());
	}
}