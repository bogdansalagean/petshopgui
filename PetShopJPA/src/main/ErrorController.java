package main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ErrorController implements Initializable {
	
	@FXML private Button retry;
	/**
	 * after pressing the retry button we swap back to the logIn scene
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		retry.setOnMouseClicked(event -> {
			try {
				Scene currentScene=retry.getScene();
				currentScene.getWindow().hide();
				
				FXMLLoader fxmlLoader = new FXMLLoader();
				fxmlLoader.setLocation(getClass().getResource("LogIN.fxml"));
				Scene scene = new Scene(fxmlLoader.load());
				Stage stage = new Stage();
				stage.setTitle("Log In");
				stage.setScene(scene);
				stage.show();
		    
		    } catch (IOException e) {
		    	System.out.println("Failed to create a new window.");
		    }
		});
	}
}