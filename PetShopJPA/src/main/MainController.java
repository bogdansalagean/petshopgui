package main;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import model.Appointment;
import util.AnimalUtil;
import util.AppointmentUtil;

public class MainController implements Initializable{
	 @FXML
	    private TextField Owner;

	    @FXML
	    private ListView<Appointment> Appointments=new ListView<>();

	    @FXML
	    private ImageView Picture;
	    
	    @FXML
	    private ListView<String> History = new ListView<String>();

	    @FXML
	    private TextField Species;

	    @FXML
	    private TextField Age;

	    @FXML
	    private TextField Name;
	    
	    @FXML
	    void closeWindow(ActionEvent event) {
	    	Platform.exit();
	    }
	    
	    /**
	     * created observable list in which all the appointments will be added
	     */
	    
	    private ObservableList<Appointment> appointmentList = FXCollections.observableArrayList();
		AppointmentUtil dbUtil=new AppointmentUtil();
		AnimalUtil dbAnimal= new AnimalUtil();

		@Override
		public void initialize(URL location, ResourceBundle resources) {
			
			/**
			 * displaying all the data in GUI
			 */
			
			appointmentList.addAll(dbUtil.listAppointments());
			Appointments.setItems(appointmentList);
			
			Appointments.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
				Species.setText(newValue.getAnimal().getSpecies());
				Name.setText(newValue.getAnimal().getName());
				Owner.setText(newValue.getAnimal().getOwner());
				Age.setText(newValue.getAnimal().getAge());
				Picture.setImage(dbAnimal.listImages(newValue));
				History.getItems().setAll(newValue.getAnimal().getHistory().split(","));
			});
			
			Appointments.setCellFactory(list -> new AppointmentCell());
	}

		/**
		 * GUI cells in which the appointments will be retained
		 */
		
	static class AppointmentCell extends ListCell<Appointment> {
		@Override
		protected void updateItem(Appointment item, boolean bln) {
			super.updateItem(item, bln);
			if (item != null)
				setText("Programare " + item.getIdappointments());
		    }
		}
	}

