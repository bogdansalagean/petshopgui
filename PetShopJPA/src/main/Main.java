package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.DatabaseGeneric;
import util.DatabaseUtil;

public class Main  extends Application {
	
	/**
	 * loading our first scene
	 */
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("LogIN.fxml"));
		Parent parent = loader.load();
		
		Scene scene = new Scene(parent);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) throws Exception {
		@SuppressWarnings("static-access")
		DatabaseGeneric<DatabaseUtil> db = new DatabaseGeneric<>().getInstance();
		
		launch(args);
		
		db.closeEntity();
	}

}
