package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the medicalemployees database table.
 * 
 */
@Entity
@Table(name="medicalemployees")
@NamedQuery(name="Medicalemployee.findAll", query="SELECT m FROM Medicalemployee m")
public class Medicalemployee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idEmployees;

	private String name;

	public Medicalemployee() {
	}

	public int getIdEmployees() {
		return this.idEmployees;
	}

	public void setIdEmployees(int idEmployees) {
		this.idEmployees = idEmployees;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}