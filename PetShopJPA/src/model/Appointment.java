package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the appointments database table.
 * 
 */
@Entity
@Table(name="appointments")
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idappointments;


	@Override
	public String toString() {
		return "Appointment [diagnosys=" + diagnosys + "]";
	}

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private String diagnosys;

	private int idMedicalEmployees;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimals")
	private Animal animal;

	public Appointment() {
	}

	public int getIdappointments() {
		return this.idappointments;
	}

	public void setIdappointments(int idappointments) {
		this.idappointments = idappointments;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDiagnosys() {
		return this.diagnosys;
	}

	public void setDiagnosys(String diagnosys) {
		this.diagnosys = diagnosys;
	}

	public int getIdMedicalEmployees() {
		return this.idMedicalEmployees;
	}

	public void setIdMedicalEmployees(int idMedicalEmployees) {
		this.idMedicalEmployees = idMedicalEmployees;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

}