package util;

import java.util.List;

import model.Medicalemployee;

public class MedicalemployeeUtil extends DatabaseUtil{


	/**
	 *  creates medicalEmployee
	 * @param medicalEmployee medicalEmployee to persist into the database
	 */
	public void saveMedicalEmployee(Medicalemployee medicalEmployee) {
		entityManager.persist(medicalEmployee);
	}
	

	/**
	 * reads all medicalEmployees
	 */
	public void printAllMedicalEmployeesFromDB() {
		@SuppressWarnings("unchecked")
		List<Medicalemployee> results= entityManager.createNativeQuery("Select * from PetShop.Medicalemployees", Medicalemployee.class)
				.getResultList();
		for (Medicalemployee medicalEmployee : results) {
			System.out.println("Doctor : " + medicalEmployee.getName() + " has ID : " + medicalEmployee.getIdEmployees());
		}
	}

	/**
	updates a certain medicalEmployee
	 * @param medicalEmployee medicalEmployee to be updated
	 * @param name name to be updated
	 * @param id id to be updated
	 */
	public void updateMedicalEmployee(Medicalemployee medicalEmployee, String name, int id) {
		  entityManager.getTransaction().begin();
		  medicalEmployee.setName(name);
		  medicalEmployee.setIdEmployees(id);
		  entityManager.getTransaction().commit();
	}
	/**
	 * delete employee
	 * @param medicalEmployee medicalEmployee to be deleted
	 */
	public void deleteMedicalEmployee(Medicalemployee medicalEmployee) {
		  entityManager.getTransaction().begin();
		  entityManager.remove(medicalEmployee);
		  entityManager.getTransaction().commit();
	}

	
}
