package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabaseUtil {
	
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public static void setUp() throws Exception {
		entityManagerFactory=Persistence.createEntityManagerFactory("PetShopJPA");
		entityManager=entityManagerFactory.createEntityManager();
	}
	public static void startTransaction() {
		entityManager.getTransaction().begin();
	}
	public static void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	public static void closeEntityManager() {
		entityManager.close();
	}
}
