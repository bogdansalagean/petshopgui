package util;

public class DatabaseGeneric<T extends DatabaseUtil> {
	
	/**
	 * creating instance 
	 */
	
	private static DatabaseGeneric<DatabaseUtil> instance;
	
	/**
	 * initializing the instance as a new database
	 */
	
	static {
		try {
			instance=new DatabaseGeneric<>();
		} catch (Exception e) {
			System.out.println("ERROR!");
		}
	}
	
	public static DatabaseGeneric<DatabaseUtil> getInstance(){
		return instance;
	}
	
	public DatabaseGeneric() throws Exception {
		DatabaseUtil.setUp();
		DatabaseUtil.startTransaction();
		DatabaseUtil.commitTransaction();
	}

	/**
	 * save the data in database
	 * @param db
	 */
	public void addToDB(DatabaseGeneric<T> db) {
		DatabaseUtil.entityManager.persist(db);
	} 
	
	public void closeEntity() {
		DatabaseUtil.closeEntityManager();
	}

}
