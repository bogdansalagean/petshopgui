package util;

import java.io.ByteArrayInputStream;
import java.util.List;

import javafx.scene.image.Image;
import model.Animal;
import model.Appointment;

public class AnimalUtil extends DatabaseUtil {

	
	/**
	 *  creates animal
	 * @param animal animal to persist into the database
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
		}
	
	/**
	 *  reads all animals
	 */
	public void printAllAnimalsFromDB() {
		@SuppressWarnings("unchecked")
		List<Animal> results= entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class)
				.getResultList();
		for (Animal animal : results) {
			System.out.println("Animal : " + animal.getName() + " has ID : " + animal.getIdAnimal());
		}
	}
	/**
	 * updates a certain animal
	 * @param animal animal to be updated
	 * @param name name to be updated
	 * @param id id to be updated
	 */
	public void updateAnimal(Animal animal, String name, int id) {
		entityManager.getTransaction().begin();
		animal.setName(name);
		animal.setIdAnimal(id);
		entityManager.getTransaction().commit();
	}
	/**
	 *  delete animal
	 * @param animal animal to be deleted
	 */
	public void deleteAnimal(Animal animal) {
		  entityManager.getTransaction().begin();
		  entityManager.remove(animal);
		  entityManager.getTransaction().commit();
	}
	public Image listImages(Appointment value) {
		Image img = null;
		img = new Image(new ByteArrayInputStream(value.getAnimal().getImage()));
		
		return img;
		}
}
