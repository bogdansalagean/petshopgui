package util;

import java.util.List;

import model.Appointment;

public class AppointmentUtil extends DatabaseUtil {

	/**
	 *  creates appointments
	 * @param appointments to persist into the database
	 */
	public void saveAppointment(Appointment appointments) {
		entityManager.persist(appointments);
	}
	
	@SuppressWarnings("unchecked")
	public List<Appointment> listDiagnosys(){
		return entityManager.createNativeQuery("Select * from PetShop.Animal, PetShop.appointments where PetShop.Animal.idAnimal=PetShop.appointments.idAnimal" , Appointment.class)
				.getResultList();
	}
	@SuppressWarnings("unchecked")
	public List<Appointment> listAppointments(){
		return entityManager.createNativeQuery("Select * from PetShop.appointments",Appointment.class)
				.getResultList();
	}
}
