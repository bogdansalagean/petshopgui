package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Animal;

class ClassTest {

	@Test
	void testAnimal() {
		Animal animalTest=new Animal();
		animalTest.setName("Rufus");
		animalTest.setSpecies("dog");
		animalTest.setIdAnimal(1);
		animalTest.setOwner("Bobo");
		animalTest.setHistory("broken leg");
		
		assertEquals("Rufus",animalTest.getName());
		assertEquals("dog",animalTest.getSpecies());
		assertEquals(1,animalTest.getIdAnimal());
		assertEquals("Bobo",animalTest.getOwner());
		assertEquals("broken leg",animalTest.getHistory());
		
	}

}
