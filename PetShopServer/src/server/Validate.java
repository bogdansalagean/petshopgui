package server;

import java.util.HashMap;
import java.util.Map;

public class Validate {
	private Map<String, String> keys;
	public Validate() {
		keys=new HashMap<String,String>();
		
		keys.put("bobo","bobo");
	}
	/**
	 *  verify if the password introduced matches the pre-given one if so
	 *  checks if the username is the same aswell
	 * @param user
	 * @param pass
	 * @return
	 */
	public boolean validate(String user, String pass) {
		return keys.containsKey(pass) && keys.get(pass).equals(user);
	}

}
