package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Echoer extends Thread {
	private Socket socket;
	private Validate validator;
	
	public Echoer(Socket socket) {
		this.socket = socket;
		validator = new Validate();
	}
	public boolean validate1(String user,String pass) {
		return validator.validate(pass, user);
	}
	
	/**
	 *verify if the data introduced into the server matches the one from the validate class
	 *returning a message which depends on the case
	 */
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			
			String user = input.readLine();
			String pass = input.readLine();
			
			if(validate1(user, pass))
				output.println("Access granted");
			else
				output.println("Access denied!");
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
